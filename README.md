# Space-Invaders JavaFX
Aquest projecte està basat en un altre tutorial javaFX descarregat desde gitHub. 
Es demana que modifiqueu aquest projecte per afegir:
- icones diferents (busqueu altres naus per posar)
- guardar rècord de punts (sols una persona) amb un fitxer json
- hi haurà un missatge final cada cop que es supere el rècord
- crear els arxius executables i que es pugui executar en qualsevol ordinador

Algunes captures de pantalla:

# Algunes naus noves i l'espai per marcador de rècord:

![space](images/01-space.jpg)




# Missatge - avís de quan es supera el rècord:

![space](images/02-space.png)




# Canviem el marcador de rècord quan aquest és superat:

![space](images/03-space.png)




# El fitxer json serà un fitxer ocult, guardat dinàmicament en un directori ocult:

![space](images/04-space.png)


## -----------------

## PROJECTE ORIGINAL:
podeu trobar la font del projecte original en aquest lloc:

[Space-Invaders](https://github.com/Gaspared/Space-Invaders)

# Space-Invaders
A simple Space Invaders game in JavaFX. For more information visit my YouTube channel

## https://www.youtube.com/watch?v=0szmaHH1hno
![space](preview.jpg)


## ALTRES PROJECTES:
Aquest repositori, inclou també altres jocs javaFX per practicar 
[Altres projectes](https://github.com/Gaspared?tab=repositories)


## Contribució
Si vols contribuir, si us plau, fes un *fork* del repositori i envia una sol·licitud de *pull request* amb les teves millores.

## Llicència
Aquest projecte està llicenciat sota la [Llicència MIT](LICENSE).



