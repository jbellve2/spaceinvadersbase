module vicent.bellver.spaceinvaders {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.base;


    opens vicent.bellver.spaceinvaders to javafx.fxml;
    exports vicent.bellver.spaceinvaders;
}